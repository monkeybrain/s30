const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Syntax:
	//mongoose.connect('<connection string>, {middlewares')

mongoose.connect('mongodb+srv://admin:admin@cluster0.ncixq.mongodb.net/session30?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
);	

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Connection Error"))

	db.once('open', () => console.log('Connected to the cloud database'))


//Mongoose Schema

/*const taskSchema = new mongoose.Schema({

	//define the name of our schema -taskSchema
	//new mongoose.Schema method - to make new schema

	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});


const Task = mongoose.model('Task', taskSchema);*/

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
-if the task already exists in the db, we return an error
-if the task doesnt exist in the db, we add it in the database
2. The task data will be coming from the request body.
3. Create a new Task object with name field property

*/

/*app.post('/tasks', (req, res) =>{

	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name === req.body.name){
			return res.send('Duplicate task found.')
		} else {
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send('New Task Created')
				}
			})
		}
	})
})

//Retrieving all tasks

app.get('/tasks', (req, res) => {

	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})*/


//Activity

const personSchema = new mongoose.Schema({



	username: String,
	//password: String,
	status: {
		type: String,
		default: 'pending'
	}
});


const Person = mongoose.model('Person', personSchema);


app.post('/signup', (req, res) => {

	Person.findOne({username: req.body.username}, (err, result) =>{
		if(result != null && result.username === req.body.username){
			return res.send('Duplicate user found.')
		} else {
			let newPerson = new Person({
				username: req.body.username
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send('New User Created')
				}
			})
		}
	})
})

//Retrieving all tasks

app.get('/users', (req, res) => {

	User.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is happily running at port ${port}`));